var Hgn = require('hogan.js'),
    mailConstant = require('./mail-constants');
module.exports = {
    formMailBody: function (mailBody, data) {
        var body = mailConstant[mailBody];
        return Hgn.compile(body).render(data);
    },
    getDateWithoutTimeStamp: function (startDate) {

        var fromDate = startDate.split("/");
        startDate = fromDate[1] + '/' + fromDate[0] + '/' + fromDate[2];
        var date = new Date(startDate);
        var day1 = date.getDate();
        var month1 = date.getMonth() + 1;
        var year1 = date.getFullYear();
        var date1 = month1 + "/" + day1 + "/" + year1;
        var resultDate = new Date(date1);
        return resultDate;
    },
    getFormattedDate: function (startDate) {
        var date = new Date(startDate);
        var day1 = date.getDate();
        var month1 = date.getMonth() + 1;
        var year1 = date.getFullYear();
        var date1 = day1 + "-" + month1 + "-" + year1;
        return date1;
    }
};
