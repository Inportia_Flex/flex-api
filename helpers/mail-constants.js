module.exports = {
    REGISTRATION_SUBJECT: 'Account Confirmation Link',
    REGISTRATION_BODY: '<p>Hi {{username}}, </p> <br> <span>Please click <a href="{{confirmationLink}}"> here </a> to confirm your account.</span>',
    PASSWORD_RESET_SUBJECT: 'Password Reset Code',
    PASSWORD_RESET_BODY: '<p>Hi {{firstName}}, </p> <br> <span> Your password  reset Code is  <b>{{verificationCode}}</b>.</span>',
    STATUS_UPDATE_SUBJECT: 'Account Status changed',
    STATUS_UPDATE_BODY: '<p>Hi {{firstName}},</p><p>Your account status is changed to {{status}}.</p>',
};
