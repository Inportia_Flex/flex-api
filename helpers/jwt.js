var JWT = require('jsonwebtoken'),

    jwtSecret = require('../config/secrets').jwtSecret,
    stringConstant = require('../helpers/string-constants');

module.exports = {
    getConfirmationToken: function (user) {
        return JWT.sign(user, jwtSecret, {
            expiresIn: '24h'
        });
    },

    getResetPasswordToken: function (email) {
        return JWT.sign({email: email}, jwtSecret, {
            expiresIn: '24h'
        });
    },

    getLoginToken: function (user) {
        return JWT.sign(user, jwtSecret, {
            expiresIn: '24h'
        });
    },

    isValidToken: function (token, callback) {
        JWT.verify(token, jwtSecret, callback);
    },

    decodeToken: function (token) {
        return JWT.decode(token);
    },

    getUserFromToken: function (token) {
        var decoded = JWT.decode(token);
        return decoded._doc;
    }

};


