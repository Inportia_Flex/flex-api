var Mongoose = require('mongoose'),
    fs = require('fs'),
    crypto = require('crypto');

var OrganisationSchema = Mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    logoURL: {
        type: String,
        default: './orgLogos/defaultLogo.png'
    },
    pan: {
        type: String,
        default: ''
    },
    tan: {
        type: String,
        default: ''
    },
    country: {
        type: String,
        default: ''
    },
    state: {
        type: String,
        default: ''
    },
    city: {
        type: String,
        default: ''
    },
    address: {
        type: String,
        default: ''
    },
    landline: {
        type: String,
        default: ''
    },
    fax: {
        type: String,
        default: ''
    },
    mobile: {
        type: String,
        default: ''
    },
    whatsapp: {
        type: Boolean,
        default: false
    },
    alternateMobile: {
        type: String,
        default: ''
    },
    alternateWhatsapp: {
        type: Boolean,
        default: false
    },
    planType: {
        type: String,
        default: ''
    },
    planStartDate: {
        type: String,
        default: ''
    },
    planExpiryDate: {
        type: String,
        default: ''
    }
}, {timestamps: true});

var Organisation = Mongoose.model('Organisation', OrganisationSchema);

Organisation.addOrganisation = function (newOrganisation, callback) {
    newOrganisation.save(callback);
};

Organisation.getAllOrganisations = function (callback) {
    Organisation.find({}, callback);
};

Organisation.deleteOrganisation = function (orgId, callback) {
    Organisation.find(
        {'_id': Mongoose.Types.ObjectId(orgId)}
    ).remove(callback);
};

Organisation.viewOrganisation = function (orgId, callback) {
    Organisation.findById(orgId, callback);
};

Organisation.uploadLogo = function (data, OrgID, callback) {
    crypto.randomBytes(10, function (err, buffer) {
        var token = buffer.toString('hex');
        var imagePath = process.env.LOGO_UPLOAD_DEST + token + '.' + data.fileType;
        fs.writeFile(imagePath, data.base64, {encoding: 'base64'}, function (err) {
            if (!err) {
                Organisation.findOne({_id: Mongoose.Types.ObjectId(OrgID)}).exec(function (err, Organisation) {
                    if (!err) {
                        Organisation.logoURL = imagePath;
                        Organisation.save(callback);
                    }
                });
            }
        });
    });
};

module.exports = Organisation;