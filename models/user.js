var Mongoose = require('mongoose'),
    Bcrypt = require('bcryptjs'),
    fs = require('fs'),
    crypto = require('crypto'),
    config = require('../config/config'),
    util = require('../helpers/util');

var UserSchema = Mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        trim: true
    },
    lastName: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    organisation: {
        type: Mongoose.Schema.ObjectId,
        ref: 'Organisation'
    },
    password: {
        type: String
    },
    state: {
        type: String,
        trim: true,
        default: ''
    },
    city: {
        type: String,
        trim: true,
        default: ''
    },
    address: {
        type: String,
        trim: true,
        default: ''
    },
    isActive: {
        type: Boolean,
        default: false
    },
    isVerified: {
        type: Boolean,
        default: false
    },
    role: {
        type: String,
        default: ''
    },
    confirmationToken: {
        type: String,
        default: ''
    },
    passwordResetCode: {
        type: String,
        default: ''
    },
    profileImageURL: {
        type: String,
        default: './profileUploads/default.png'
    },
    mobile: {
        type: String,
        default: ''
    },
    whatsapp: {
        type: Boolean,
        default: false
    }
}, {timestamps: true});

var User = Mongoose.model('User', UserSchema);

User.createUser = function (newUser, callback) {
    Bcrypt.genSalt(10, function (err, salt) {
        Bcrypt.hash(newUser.password, salt, function (err, hash) {
            if (err) {
                apiHandler.setErrorResponse('ERROR_WHILE_ECRYPTING_PASSWORD', res);
            } else {
                newUser.password = hash;
                newUser.save(callback);
            }
        });
    });
};

User.getUserByEmail = function (email, callback) {
    User.findOne({email: email}, callback);
};

User.getUserByConfirmationToken = function (token, callback) {
    User.findOne({confirmationToken: token}, callback);
};

User.getUserByPasswordResetCode = function (passwordResetCode, callback) {
    User.findOne({passwordResetCode: passwordResetCode}, callback);
};

User.getUserById = function (id, callback) {
    User.findById(id, callback);
};

User.getAllUsers = function (callback) {
    User.find({},
        {
            'password': 0,
            'confirmationToken': 0,
            'passwordResetCode': 0
        }, callback);
};

User.comparePassword = function (candidatePassword, hash, callback) {
    Bcrypt.compare(candidatePassword, hash, function (err, isMatch) {
        if (err) {
            throw  err;
        }
        callback(null, isMatch);
    });
};

User.updateConfirmationToken = function (confirmationToken, callback) {
    User.update({$set: {confirmationToken: confirmationToken}}, callback);
};

User.viewProfile = function (id, callback) {
    User.findOne({_id: id}, {
        'password': 0,
        'isActive': 0,
        'updatedAt': 0,
        '__v': 0,
        'isVerified': 0,
        'confirmationToken': 0,
        'passwordResetCode': 0
    }, callback);
};
User.uploadPhoto = function (data, UserID, callback) {
    crypto.randomBytes(10, function (err, buffer) {
        var token = buffer.toString('hex');
        var imagePath = process.env.PROFILE_UPLOAD_DEST + token + '.' + data.fileType;
        fs.writeFile(imagePath, data.base64, {encoding: 'base64'}, function (err) {
            if (!err) {
                User.findOne({_id: Mongoose.Types.ObjectId(UserID)}).exec(function (err, user) {
                    if (!err) {
                        user.profileImageURL = imagePath;
                        user.save(callback);
                    }
                });
            }
        });
    });
};

User.deleteUser = function (userId, callback) {
    User.find(
        {'_id': Mongoose.Types.ObjectId(userId)}
    ).remove(callback);
};

module.exports = User;