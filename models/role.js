var Mongoose = require('mongoose');

var roleSchema = Mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        unique:true
    }
}, { timestamps: true });

var role = Mongoose.model('role', roleSchema);

role.createRole = function (newRole, callback) {
    newRole.save(callback);
};

role.getAllRoles = function (callback) {
    role.find({}, callback);
};

role.deleteRole = function (name, callback) {
    role.remove({name: name}, callback)
};

role.getRoleByName = function (name, callback) {
    role.find({ name: name }, callback );
};

module.exports = role;