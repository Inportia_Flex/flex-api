var Mongoose = require('mongoose'),
    fs = require('fs'),
    crypto = require('crypto');

var OrderSchema = Mongoose.Schema({
    orderName: {
        type: String,
    },
    representativeName: {
        type: String,
    },
    user: {
        type: Mongoose.Schema.ObjectId,
        ref: 'User'
    },
    customer: {
        type: Mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    suborders: [
        {   
            suborderid: {
                type: String
            },
            height: {
                type: String
            },
            orderType: {
                type: String
            },
            width: {
                type: String
            },
            area: {
                type: String
            },
            quantity: {
                type: String
            },
            customDesign: {
                type: Boolean,
                default: false
            },
            designImageUrl: {
                type: String,
                default: './designs/defaultDesign.png'
            }
        }
    ],
    homeDelivery: {
        type: Boolean,
        default: false
    },
    homeDeliveryAddress: {
        type: String
    },
    pastingOnSite: {
        type: Boolean,
        default: false
    },
    pastingOnSiteAddress: {
        type: String
    },
    deliveryDate: { type: Date },
    currentDate: { type: Date, default: Date.now },
    totalPrice: {
        type: String
    }
}, {timestamps: true});

var Order = Mongoose.model('Order', OrderSchema);

Order.addOrder = function (newOrder, callback) {
    newOrder.save(callback);
};

Order.getAllOrders = function (callback) {
    Order.find({}, callback);
};

Order.deleteOrder = function (orderId, callback) {
    Order.find(
        {'_id': Mongoose.Types.ObjectId(orderId)}
    ).remove(callback);
};

Order.viewOrder = function (orderId, callback) {
    Order.findById(orderId, callback);
};


Order.uploadDesignImage = function (data, orderId, callback) {
    crypto.randomBytes(10, function (err, buffer) {
        var token = buffer.toString('hex');
        var imagePath = process.env.DESIGN_UPLOAD_DEST + token + '.' + data.fileType;
        fs.writeFile(imagePath, data.base64, {encoding: 'base64'}, function (err) {
            if (!err) {
                Order.findOne({_id: Mongoose.Types.ObjectId(orderId)}).exec(function (err, Order) {
                    if (!err) {
                        Order.designImageUrl = imagePath;
                        Order.save(callback);
                    }
                });
            }
        });
    });
};

module.exports = Order;