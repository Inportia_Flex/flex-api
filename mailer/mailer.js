var Nodemailer = require('nodemailer'),
    Promise = require('bluebird'),
    mailConfig = require('../config/config'),
    mailConstants = require('../helpers/mail-constants'),
    util = require('../helpers/util');
var transporter = Nodemailer.createTransport([
    'smtps://',
    process.env.MAIL_SERVER_USER_ID,
    ':',
    process.env.MAIL_SERVER_USER_PASSWORD,
    '@smtp.gmail.com'
].join(''));

console.log('process.env.MAIL_SERVER_USER_ID', process.env.MAIL_SERVER_USER_ID);
console.log('process.env.MAIL_SERVER_USER_PASSWORD', process.env.MAIL_SERVER_USER_PASSWORD);
module.exports = {
    sendConfirmationMail: function (username, email, confirmationUrl) {
        var confirmationLink = process.env.APP_CLIENT_BASE_URL_PROD + confirmationUrl,
            mailBody = util.formMailBody('REGISTRATION_BODY', {
                username: username,
                confirmationLink: confirmationLink
            }),
            mailOptions = {
                from: process.env.MAIL_SERVER_USER_ID,
                to: email,
                subject: mailConstants.REGISTRATION_SUBJECT,
                html: mailBody
            };
        return new Promise(function (resolve, reject) {
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(info.response);
                }
            });
        });
    },
    sendPasswordResetMail: function (email, verificationCode, firstName) {
             var mailBody = util.formMailBody('PASSWORD_RESET_BODY', {
                firstName: firstName,
                     verificationCode: verificationCode
            }),
            mailOptions = {
                from: process.env.MAIL_SERVER_USER_ID,
                to: email,
                subject: mailConstants.PASSWORD_RESET_SUBJECT,
                html: mailBody
            };
        return new Promise(function (resolve, reject) {
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(info.response);
                }
            });
        });
    },
    sendUpdateStatusMail: function (users) {
        var status;
        if (users.isActive) {
            status = "Active";
        } else {
            status = "Inactive";
        }
        mailBody = util.formMailBody('STATUS_UPDATE_BODY', {
            firstName: users.firstName,
            status: status
        }),
            mailOptions = {
                from: process.env.MAIL_SERVER_USER_ID,
                to: users.email,
                subject: mailConstants.STATUS_UPDATE_SUBJECT,
                html: mailBody
            };
        return new Promise(function (resolve, reject) {
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(info.response);
                }
            });
        });
    }
};