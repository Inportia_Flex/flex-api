module.exports = {
    jwtSecret: 'test-token',
    allowedOrigins: '*',
    allowedMethods: 'GET, POST, PUT, DELETE, OPTIONS',
    allowedHeaders: 'Cache-Control, Content-Type, Accept, token',
    exposedHeaders: 'Cache-Control, Content-Type, Accept, token',
    preFlightRequestMaxAge: '1728000'
};
