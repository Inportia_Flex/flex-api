var apiVersion = 'v1';

module.exports = {
    apiVersion: apiVersion,
    policies: {
        superAdmin: ["/addOrganisation", "/getAllOrganisations", "/deleteOrganisation", "/updateOrganisation", "/createRole", "/getAllRoles", "/deleteRole"],
        admin: ["/uploadLogo", "/addUser", "/getAllUsers", "/deleteUser"],
        user: ["/viewOrganisation", "/updateUser", "/login", "/logout", "/changePassword", "/forgotPassword", "/resetPassword", "/confirm", "/activate/:id", "/deactivate/:id", "/viewProfile", "/uploadPhoto", "/countries", "/me", "/tokenCheck"],
        registeredCustomer: ["/getAllOrders", "/updateOrder", "/deleteOrder", "/viewOrder", "/uploadDesignImage"],
        unRegisteredCustomer: ["/addOrder"]
    },
    excludedRoutes: [
        '/' + apiVersion + '/admin/createRoles',
        '/' + apiVersion + '/admin/createSuperAdmin',

        '/' + apiVersion + '/users/login',
        '/' + apiVersion + '/users/addUser',
        '/' + apiVersion + '/users/forgotPassword',
        '/' + apiVersion + '/users/confirm',
        '/' + apiVersion + '/users/resetPassword',
        '/' + apiVersion + '/users/tokenCheck',
        '/' + apiVersion + '/roles/createRole',
        '/' + apiVersion + '/users/deleteUser'
    ]
};
