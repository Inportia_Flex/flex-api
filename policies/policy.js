'use strict';

var appConfig = require('../config/config');
var superAdminAllowed = appConfig.policies.superAdmin, adminAllowed = appConfig.policies.admin, userAllowed = appConfig.policies.user, registeredCustomerAllowed = appConfig.policies.registeredCustomer, unRegisteredCustomerAllowed = appConfig.policies.unRegisteredCustomer;

exports.isAllowed = function (req, res, next) {
    console.log('heere');
    var roles = req.user.role,
        superAdminRouteCheck = (superAdminAllowed.indexOf(req.route.path) >= 0),
        adminRouteCheck = (adminAllowed.indexOf(req.route.path) >= 0),
        userRouteCheck = (userAllowed.indexOf(req.route.path) >= 0),
        registeredCustomerRouteCheck = (registeredCustomerAllowed.indexOf(req.route.path) >= 0),
        unRegisteredCustomerRouteCheck = (unRegisteredCustomerAllowed.indexOf(req.route.path) >= 0);
    if (roles.indexOf("superAdmin") >= 0 && (superAdminRouteCheck || adminRouteCheck || userRouteCheck || registeredCustomerRouteCheck || unRegisteredCustomerRouteCheck)) {
        return next();
    } else if (roles.indexOf("admin") >= 0 && (adminRouteCheck || userRouteCheck || registeredCustomerRouteCheck || unRegisteredCustomerRouteCheck)) {
        return next();
    } else if (roles.indexOf("user") >= 0 && (userRouteCheck || registeredCustomerRouteCheck || unRegisteredCustomerRouteCheck)) {
        return next();
    } else if (roles.indexOf("registeredCustomer") >= 0 && (registeredCustomerRouteCheck || unRegisteredCustomerRouteCheck)) {
        return next();
    } else if (roles.indexOf("unRegisteredCustomer") >= 0 && unRegisteredCustomerRouteCheck) {
        console.log('in here');
        return next();
    } else {
        return res.status(403).json({message: 'User is not authorized'});
    }
};
