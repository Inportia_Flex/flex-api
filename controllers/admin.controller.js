var apiHandler = require('../services/api-handler'),
    models = require('../models/index');


exports.createRoles = function (req, res) {
    var rolesToBeAdded = ['superAdmin', 'admin', 'user', 'registeredCustomer', 'unRegisteredCustomer'];

    var stop = false;
        rolesToBeAdded.forEach(function (role) {
            if (!stop) {
                var newRole = new models.roles({name: role});
                models.roles.createRole(newRole, function (err, rolesData) {
                    if (err) {
                        stop = true;
                    }
                });
            }
        });
        res.send('done');

};

exports.createSuperAdmin = function (req, res) {
    var bodyParams = {};
    bodyParams.email = 'inportia.flex@gmail.com';
    bodyParams.password = 'inportia123';
    bodyParams.firstName = 'Inportia';
    bodyParams.lastName = 'Solutions';
    bodyParams.role = ['superAdmin'];

    var newUser = new models.users(bodyParams);
    
    models.users.createUser(newUser, function (err, user) {
        if (err) {
            apiHandler.setErrorResponse('UNKNOWN_ERROR', res);
        } else {
            // var token = jWT.getLoginToken(user);

            var data = {
                // token: token,
                user: user
            };

            apiHandler.setSuccessResponse(data, res);
        }
    });
};
    