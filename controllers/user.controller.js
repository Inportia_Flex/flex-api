var Passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    apiHandler = require('../services/api-handler'),
    stringConstant = require('../helpers/string-constants'),
    countryHelper = require('../helpers/country'),
    jWT = require('../helpers/jwt'),
    mailer = require('../mailer/mailer'),
    JWT = require('jsonwebtoken'),
    jwtSecret = require('../config/secrets').jwtSecret,
    utility = require('../helpers/util'),
    models = require('../models/index'),
    bcrypt = require('bcryptjs'),
    randomstring = require("randomstring");


exports.addUser = function (req, res) {
    var bodyParams = req.body,
        name = bodyParams.firstName + ' ' + bodyParams.lastName,
        email = bodyParams.email,
        password = bodyParams.password,
        confirmPassword = bodyParams.confirmPassword,
        emailRegex = new RegExp(stringConstant.VALID_EMAIL_REGEX),
        firstName = '', lastName = '', fullName = '', newUser, confirmationToken, confirmationUrl;
    if (!name || !email || !password) {
        apiHandler.setErrorResponse('REQUIRED_FIELDS_MISSING', res);
    } else if (!emailRegex.test(email)) {
        apiHandler.setErrorResponse('INVALID_EMAIL_ADDRESS', res);
    } else if (password && password !== confirmPassword) {
        apiHandler.setErrorResponse('CONFIRM_PASSWORD_NOT_SAME', res);
    } else {
        fullName = name.split(' ');
        if (fullName.length > 1) {
            lastName = fullName[fullName.length - 1];
            for (var i = 0; i < fullName.length - 1; i++) {
                firstName = firstName + ' ' + fullName[i];
            }
        } else {
            firstName = fullName[0];
            lastName = '';
        }
        bodyParams.firstName = firstName;
        bodyParams.lastName = lastName;

        models.roles.getRoleByName(bodyParams.roleName, function (err, roleData) {
            if (err) {
                apiHandler.setErrorResponse('DB_EMPTY', res);
            } else {
                bodyParams.role = [roleData[0].name];
                newUser = new models.users(bodyParams);
                models.users.createUser(newUser, function (err, user) {
                    if (err) {
                        if (err.code === 11000) {
                            apiHandler.setErrorResponse('EMAIL_ALREADY_EXIST', res);
                        } else {
                            apiHandler.setErrorResponse('UNKNOWN_ERROR', res);
                        }
                    } else {
                        confirmationToken = jWT.getConfirmationToken(newUser);
                        models.users.updateConfirmationToken(confirmationToken, function (err, userDetails) {
                            if (err) {
                                apiHandler.setErrorResponse('DB_EMPTY', res);
                            } else {
                                confirmationUrl = ':3001/v1/users/confirm?token=' + confirmationToken;
                                mailer.sendConfirmationMail(firstName, email, confirmationUrl)
                                    .then(function (success) {
                                        var token = jWT.getLoginToken(user);

                                        var data = {
                                            token: token,
                                            user: user
                                        };

                                        apiHandler.setSuccessResponse(data, res);
                                    })
                                    .catch(function (err) {
                                        user.remove();
                                        console.log('errrrrrrrrrrrrr', err);
                                        apiHandler.setErrorResponse('ERROR_IN_USER_ADDITION', res);
                                    });
                            }
                        });
                    }
                });
            }
        });
    }
};

Passport.use(new LocalStrategy(
    function (email, password, done) {
        models.users.getUserByEmail(email, function (err, user) {
            if (err) {
                return done(null, false, {message: 'INVALID_USER'});
            }
            if (!user) {
                return done(null, false, {message: 'INVALID_USER'});
            }
            models.users.comparePassword(password, user.password, function (err, isMatch) {
                if (err) {
                    return done(null, false, {message: 'UNKNOWN_ERROR'});
                } else {
                    if (isMatch) {
                        return done(null, user);
                    } else {
                        return done(null, false, {message: 'PASSWORD_DOES_NOT_MATCH'});
                    }
                }
            });
        });
    }
));

exports.login = function (req, res, next) {

    Passport.authenticate('local', function (err, user, info) {
        var token, newToken;
        if (user) {
            token = jWT.getLoginToken(user);
            var data = {token: token, user: user};
            apiHandler.setSuccessResponse(data, res);
        } else {
            apiHandler.setErrorResponse('LOGIN_FAILED', res);
        }
    })(req, res, next);
};

Passport.serializeUser(function (user, done) {
    done(null, user.id);
});

Passport.deserializeUser(function (id, done) {
    models.users.getUserById(id, function (err, user) {
        done(err, user);
    });
});

exports.me = function (req, res) {
    var user = req.user;
    apiHandler.setSuccessResponse(user, res);
};

exports.getAllUsers = function (req, res) {
    models.users.getAllUsers(apiHandler.handler.check(res, 'DB_EMPTY', function (users) {
        apiHandler.setSuccessResponse(users, res);
    }));
};

exports.activateId = function (req, res) {
    models.users.getUserById(req.params.id, apiHandler.handler.check(res, 'INVALID_USER', function (user) {
        user.isActive = true;
        user.save(function (err) {
            if (err) {
                console.log('Error in User save function');
            } else {
                mailer.sendUpdateStatusMail(user)
                    .then(function (success) {
                    })
                    .catch(function (err) {
                        apiHandler.setErrorResponse('ERROR_WHILE_MAIL_SENDING', res);
                    });
            }
        });
    }));
};

exports.deactivateId = function (req, res) {
    models.users.getUserById(req.params.id, apiHandler.handler.check(res, 'INVALID_USER', function (user) {
        user.isActive = false;
        user.save(function (err) {
            if (err) {
                console.log('Error in User save function');
            } else {
                mailer.sendUpdateStatusMail(user)
                    .then(function (success) {
                    })
                    .catch(function (err) {
                        apiHandler.setErrorResponse('ERROR_WHILE_MAIL_SENDING', res);
                    });
            }
        });
    }));
};

exports.changePassword = function (req, res) {
    var reqBody = req.body,
        email = req.user.email,
        oldPassword = reqBody.oldPassword,
        newPassword = reqBody.newPassword,
        newPasswordConfirm = reqBody.newConfirmPassword;

    if (newPassword === newPasswordConfirm) {
        models.users.getUserByEmail(email, function (err, user) {
            models.users.comparePassword(oldPassword, user.password, apiHandler.handler.check(res, 'ERROR_IN_UPDATE_PASSWORD', function (isMatch) {
                if (isMatch) {
                    bcrypt.genSalt(10, function (err, salt) {
                        bcrypt.hash(newPassword, salt, function (err, hash) {
                            if (err) {
                                apiHandler.setErrorResponse('ERROR_WHILE_ENCRYPTING_PASSWORD', res);
                            } else {
                                user.password = hash;
                                user.save();
                                apiHandler.setSuccessResponse('PASSWORD_UPDATED', res);
                            }
                        });
                    });
                } else {
                    apiHandler.setErrorResponse('PASSWORD_DOES_NOT_MATCH', res);
                }
            }));
        });
    } else {
        apiHandler.setErrorResponse('CONFIRM_PASSWORD_NOT_SAME', res);
    }
};

exports.confirm = function (req, res) {
    var token = req.query.token || req.headers['x-access-token'];
    jWT.isValidToken(token, apiHandler.handler.check(res, 'INVALID_TOKEN', function (decoded) {
        models.users.getUserByConfirmationToken(token, apiHandler.handler.check(res, 'CONFIRMATION_TOKEN_EXPIRED', function (user) {
            if (user) {
                user.isVerified = true;
                user.confirmationToken = '';
                user.save();
                apiHandler.setSuccessResponse('ACCOUNT_CONFIRMATION_ACTIVITY', res);
            }
        }));
    }));
};

exports.forgotPassword = function (req, res) {
    var reqBody = req.body,
        email = reqBody.email,
        passwordResetCode;

    models.users.getUserByEmail(email, apiHandler.handler.check(res, 'INVALID_USER', function (user) {
        if (user) {
            if (user.passwordResetCode) {
                passwordResetCode = user.passwordResetCode;
            } else {
                passwordResetCode = randomstring.generate(6);
                user.passwordResetCode = passwordResetCode;
                user.save();
            }
            mailer.sendPasswordResetMail(email, passwordResetCode, user.firstName)
                .then(function (success) {
                    apiHandler.setSuccessResponse('FORGOT_PASSWORD_ACTIVITY', res);
                })
                .catch(function (err) {
                    user.remove();
                    apiHandler.setErrorResponse('ERROR_WHILE_MAIL_SENDING', res);
                });
        }
    }));
};

exports.resetPassword = function (req, res) {
    var reqBody = req.body,
        newPassword = reqBody.newPassword,
        newConfirmPassword = reqBody.newConfirmPassword,
        passwordResetCode = reqBody.passwordResetCode;
    console.log('reqBody', reqBody);
    if (newPassword === newConfirmPassword) {
        models.users.getUserByPasswordResetCode(passwordResetCode, apiHandler.handler.check(res, 'UNKNOWN_ERROR', function (user) {
            if (user && user.passwordResetCode !== passwordResetCode) {
                apiHandler.setErrorResponse('CODE_EXPIRED', res);
            } else {

                bcrypt.genSalt(10, function (err, salt) {
                    bcrypt.hash(newPassword, salt, function (err, hash) {
                        if (err) {
                            apiHandler.setErrorResponse('ERROR_WHILE_ENCRYPTING_PASSWORD', res);
                        } else {
                            user.password = hash;
                            user.passwordResetCode = '';
                            user.save();
                            apiHandler.setSuccessResponse('PASSWORD_UPDATED', res);
                        }
                    });
                });
            }
        }));
    } else {
        apiHandler.setErrorResponse('CONFIRM_PASSWORD_NOT_SAME', res);
    }
};

exports.tokenCheck = function (req, res) {
    var token = req.headers.token || req.headers['x-access-token']
    JWT.verify(token, jwtSecret, function (err) {
        if (!err) {
            apiHandler.setSuccessResponse('VALID_TOKEN', res);
        }
        else {
            apiHandler.setErrorResponse('INVALID_TOKEN', res);
        }
    });
};

exports.logout = function (req, res) {
    var token = req.headers.token || req.headers['x-access-token'];
    req.logout();
    apiHandler.setSuccessResponse('LOGOUT_SUCCESS', res);
};

exports.viewProfile = function (req, res) {
    var userId;
    if (req.body.userId) {
        userId = req.body.userId;
    } else {
        userId = req.user._id;
    }
    models.users.viewProfile(userId, apiHandler.handler.check(res, 'DB_EMPTY', function (profile) {
            apiHandler.setSuccessResponse(profile, res);
    }));
};

exports.updateUser = function (req, res) {
    models.users.viewProfile(req.body.userId, apiHandler.handler.check(res, 'DB_EMPTY', function (profile) {
        if (!profile) {
            apiHandler.setErrorResponse('DB_EMPTY', res);
        } else {
            profile.address = "";
            profile.city = "";
            profile.state = "";
            profile.country = "";
            var bodyParams = req.body;
            if (bodyParams.firstName) {
                profile.firstName = bodyParams.firstName;
            }
            if (bodyParams.lastName) {
                profile.lastName = bodyParams.lastName;
            }
            if (bodyParams.city) {
                profile.city = bodyParams.city;
            }
            if (bodyParams.state) {
                profile.state = bodyParams.state;
            }
            if (bodyParams.country) {
                profile.country = bodyParams.country;
            }
            if (bodyParams.address) {
                profile.address = bodyParams.address;
            }
            if (bodyParams.role) {
                profile.role = bodyParams.role;
            }

            profile.save(function (err) {
                if (!err) {
                    apiHandler.setSuccessResponse(profile, res);
                }
            });
        }
    }));
};

exports.uploadPhoto = function (req, res) {
    var img = req.body.img;
    var fileType = ((img).split(";")[0]).split("/")[1];
    var base64 = ((img).split("base64,")[1]);
    var data = {"base64": base64, "fileType": fileType};
    models.users.uploadPhoto(data, req.user._id, apiHandler.handler.check(res, 'PROFILE_UPLOAD_FAILED', function (upload) {
        apiHandler.setSuccessResponse(upload, res);
    }));
};

exports.countries = function (req, res) {
    var countries = countryHelper.countryArray;
    apiHandler.setSuccessResponse(countries, res);
};

exports.deleteUser = function (req, res) {
    models.users.deleteUser(req.body.userId, apiHandler.handler.check(res, 'DB_EMPTY', function (done) {
        apiHandler.setSuccessResponse(done, res);
    }))
};