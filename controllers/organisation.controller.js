var apiHandler = require('../services/api-handler'),
    stringConstant = require('../helpers/string-constants'),
    mailer = require('../mailer/mailer'),

    models = require('../models/index');

exports.addOrganisation = function (req, res) {
    var newOrganisation = new models.organisations(req.body);
    models.organisations.addOrganisation(newOrganisation, function (err, orgData) {
        if (!err) {
            apiHandler.setSuccessResponse(orgData, res);
        } else {
            apiHandler.setErrorResponse('NOT_AN_ADMIN_USER', res, req);
        }
    })
};

exports.getAllOrganisations = function (req, res) {
    models.organisations.getAllOrganisations(apiHandler.handler.check(res, 'DB_EMPTY', function (organisations) {
        apiHandler.setSuccessResponse(organisations, res);
    }));
};

exports.updateOrganisation = function (req, res) {
    models.organisations.viewOrganisation(req.body.orgId, apiHandler.handler.check(res, 'DB_EMPTY', function (orgData) {
        if (!orgData) {
            apiHandler.setErrorResponse('DB_EMPTY', res);
        } else {
            var bodyParams = req.body;
            
            if (bodyParams.name) {
                orgData.name = bodyParams.name;
            }
            if (bodyParams.orgEmail) {
                orgData.orgEmail = bodyParams.orgEmail;
            }
            if (bodyParams.pan) {
                orgData.pan = bodyParams.pan;
            }
            if (bodyParams.tan) {
                orgData.tan = bodyParams.tan;
            }
            if (bodyParams.country) {
                orgData.country = bodyParams.country;
            }
            if (bodyParams.state) {
                orgData.state = bodyParams.state;
            }
            if (bodyParams.city) {
                orgData.city = bodyParams.city;
            }
            if (bodyParams.country) {
                orgData.country = bodyParams.country;
            }
            if (bodyParams.landline) {
                orgData.landline = bodyParams.landline;
            }
            if (bodyParams.fax) {
                orgData.fax = bodyParams.fax;
            }
            if (bodyParams.mobile) {
                orgData.mobile = bodyParams.mobile;
            }
            if (bodyParams.whatsapp) {
                orgData.whatsapp = bodyParams.whatsapp;
            }
            if (bodyParams.alternateMobile) {
                orgData.alternateMobile = bodyParams.alternateMobile;
            }
            if (bodyParams.alternateWhatsapp) {
                orgData.alternateWhatsapp = bodyParams.alternateWhatsapp;
            }
            if(bodyParams.planType) {
                orgData.planType = bodyParams.planType;
            }
            if (bodyParams.planStartDate) {
                orgData.planStartDate = bodyParams.planStartDate;
            }
            if(bodyParams.planExpiryDate) {
                orgData.planExpiryDate = bodyParams.planExpiryDate;
            }

            orgData.save(function (err) {
                if (!err) {
                    apiHandler.setSuccessResponse(orgData, res);
                }
            });
        }
    }));
};

exports.deleteOrganisation = function (req, res) {
    models.organisations.deleteOrganisation(req.body.orgId, apiHandler.handler.check(res, 'DB_EMPTY', function (done) {
        apiHandler.setSuccessResponse(done, res);
    }))
};

exports.viewOrganisation = function (req, res) {
      var orgId = req.body.orgId;
    models.organisations.viewOrganisation(orgId, apiHandler.handler.check(res, 'DB_EMPTY', function (organisation) {
        if (!organisation) {
            apiHandler.setErrorResponse('DB_EMPTY', res);
        } else {
            apiHandler.setSuccessResponse(organisation, res);
        }
    }));
};

exports.uploadLogo = function (req, res) {
    var img = req.body.img;
    var fileType = ((img).split(";")[0]).split("/")[1];
    var base64 = ((img).split("base64,")[1]);
    var data = {"base64": base64, "fileType": fileType};
    models.organisations.uploadLogo(data, req.body.orgId, apiHandler.handler.check(res, 'PROFILE_UPLOAD_FAILED', function (upload) {
        apiHandler.setSuccessResponse(upload, res);
    }));
};