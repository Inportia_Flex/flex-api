var model = require('../models/index'),
    apiHandler = require('../services/api-handler');

exports.createRole = function (req, res) {
    console.log(req.body);
    var newRole = new model.roles({name: req.body.roleName});
    model.roles.createRole(newRole, function (err, rolesData) {
        if (!err) {
            apiHandler.setSuccessResponse({roles: rolesData}, res);
        } else {
            apiHandler.setErrorResponse('UNKNOWN_ERROR', res);
        }
    })
};

exports.getAllRoles = function (req, res) {
    models.roles.getAllRoles(apiHandler.handler.check(res, 'DB_EMPTY', function (roles) {
        apiHandler.setSuccessResponse(roles, res);
    }));
};

exports.deleteRole = function (req, res) {
    models.roles.deleteRole(req.body.roleName, apiHandler.handler.check(res, 'DB_EMPTY', function (done) {
        apiHandler.setSuccessResponse(done, res);
    }))
};