var apiHandler = require('../services/api-handler'),
    mailer = require('../mailer/mailer'),
    models = require('../models/index');

exports.addOrder = function (req, res) {
    var newOrder = new models.orders(req.body);
    models.orders.addOrder(newOrder, function (err, orderData) {
        if (!err) {
            apiHandler.setSuccessResponse(orderData, res);
        } else {
            console.log(err);
            apiHandler.setErrorResponse('NOT_AN_ADMIN_USER', res, req);
        }
    })
};

exports.getAllOrders = function (req, res) {
    models.orders.getAllOrders(apiHandler.handler.check(res, 'DB_EMPTY', function (orders) {
        apiHandler.setSuccessResponse(orders, res);
    }));
};

exports.updateOrder = function (req, res) {
    models.orders.viewOrder(req.body.orderId, apiHandler.handler.check(res, 'DB_EMPTY', function (orderData) {
        if (!orderData) {
            apiHandler.setErrorResponse('DB_EMPTY', res);
        } else {
            var bodyParams = req.body;

            if (bodyParams.representativeName) {
                orderData.representativeName = bodyParams.representativeName;
            }
            if (bodyParams.user) {
                orderData.user = bodyParams.user;
            }
            if (bodyParams.customer) {
                orderData.customer = bodyParams.customer;
            }
            if (bodyParams.orderType) {
                orderData.orderType = bodyParams.orderType;
            }
            if (bodyParams.height) {
                orderData.height = bodyParams.height;
            }
            if (bodyParams.width) {
                orderData.width = bodyParams.width;
            }
            if (bodyParams.area) {
                orderData.area = bodyParams.area;
            }
            if (bodyParams.quantity) {
                orderData.quantity = bodyParams.quantity;
            }
            if (bodyParams.customDesign) {
                orderData.customDesign = bodyParams.customDesign;
            }
            if (bodyParams.homeDelivery) {
                orderData.homeDelivery = bodyParams.homeDelivery;
            }
            if (bodyParams.homeDeliveryAddress) {
                orderData.homeDeliveryAddress = bodyParams.homeDeliveryAddress;
            }
            if (bodyParams.pastingOnSite) {
                orderData.pastingOnSite = bodyParams.pastingOnSite;
            }
            if (bodyParams.pastingOnSiteAddress) {
                orderData.pastingOnSiteAddress = bodyParams.pastingOnSiteAddress;
            }
            if (bodyParams.deliveryDate) {
                orderData.deliveryDate = bodyParams.deliveryDate;
            }
            if (bodyParams.totalPrice) {
                orderData.totalPrice = bodyParams.totalPrice;
            }
            orderData.save(function (err) {
                if (!err) {
                    apiHandler.setSuccessResponse(orderData, res);
                }
            });
        }
    }));
};

exports.deleteOrder = function (req, res) {
    models.orders.deleteOrder(req.body.orderId, apiHandler.handler.check(res, 'DB_EMPTY', function (done) {
        apiHandler.setSuccessResponse(done, res);
    }))
};

exports.viewOrder = function (req, res) {
    var orderId = req.body.orderId;
    models.orders.viewOrder(orderId, apiHandler.handler.check(res, 'DB_EMPTY', function (order) {
        if (!order) {
            apiHandler.setErrorResponse('DB_EMPTY', res);
        } else {
            apiHandler.setSuccessResponse(order, res);
        }
    }));
};

exports.uploadDesignImage = function (req, res) {
    var img = req.body.img;
    var fileType = ((img).split(";")[0]).split("/")[1];
    var base64 = ((img).split("base64,")[1]);
    var data = {"base64": base64, "fileType": fileType};
    models.orders.uploadDesignImage(data, req.body.orderId, apiHandler.handler.check(res, 'PROFILE_UPLOAD_FAILED', function (upload) {
        apiHandler.setSuccessResponse(upload, res);
    }));
};