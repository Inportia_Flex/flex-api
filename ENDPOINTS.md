All the APIs need to send following data in Headers:

Content-Type: application/json
token: (auth-token  generated when user logs in)

Following APIs have exceptions for token (Excluded routes):
    /users/login,
    /users/addUser,
    /users/forgotPassword,
    /users/confirm,
    /users/resetPassword,
    /users/tokenCheck,


Please affix API version before each route.

    API Version: v1

URL example: www.xyz.com:3001/v1/users/register

------------------------USERS----------------------------------
URL	:	/users/addUser
METHOD	:	POST
PARAMS	:
{
    "organisation": "14520214554121254",
    "firstName": "firstName",
    "lastName": "lastName",
    "email": "email",
    "password": "password",
    "country": "country",
    "state": "state",
    "city": "city",
    "address": "address",
    "roleName": ["user"],
    "gender": "gender",
    "profileImageURL": "profileImageURL",
    "mobile": "9898989898",
    "whatsapp": true,
    "identification": "identification"
}


URL	:	/users/getAllUsers
METHOD	:	GET
PARAMS	:
{}



URL	:	/users/updateUser
METHOD	:	POST
PARAMS	:
{
    "userId": "user ID",
    "firstName": "firstName",
    "lastName": "lastName",
    "email": "email",
    "organisation": "14520214554121254" //Org ID,
    "password": "password;,
    "country": "country",
    "state": "state",
    "city": "city",
    "address": "address",
    "role": ["user"], //Array of Roles you want to add or update with existing roles
    "gender": "gender",
    "profileImageURL": "profileImageURL",
    "mobile": "9898989898",
    "whatsapp": true,
    "identification": "identification"
}



URL	:	/users/deleteUser
METHOD	:	POST
PARAMS	:
{
    "userId": "user ID",
}


------------ Addiional APIs--------------------

URL	:	/users/login
METHOD	:	POST
PARAMS	:
{
    "username":"email",
    "password": "password"
}

URL	:	/users/logout
METHOD	:	POST
PARAMS	:
{}

URL	:	/users/changePassword
METHOD	:	POST
PARAMS	:
{
    "oldPassword": "oldPassword",
    "newPassword": "newPassword",
    "newConfirmPassword": "newConfirmPassword"
}

URL	:	/users/forgotPassword
METHOD	:	POST
PARAMS	:
{
    "email": "email"
}

URL	:	/users/resetPassword
METHOD	:	POST
PARAMS	:
{
    "passwordResetCode": "passwordResetCode",
    "newPassword": "newPassword",
    "newConfirmPassword": "newConfirmPassword"
}

URL	:	/users/confirm
METHOD	:	GET
PARAMS	:
{}

URL	:	/users/activate/:id
METHOD	:	GET
PARAMS	:
{}

URL	:	/users/deactivate/:id
METHOD	:	GET
PARAMS	:
{}

URL	:	/users/viewProfile
METHOD	:	POST
PARAMS	:
{
    "userId": "user ID",
}


URL	:	/users/uploadPhoto
METHOD	:	POST
PARAMS	:
{
    "img": "image"
}

URL	:	/users/countries
METHOD	:	GET
PARAMS	:
{}

URL	:	/users/me
METHOD	:	POST
PARAMS	:
{}

URL	:	/users/tokenCheck
METHOD	:	POST
PARAMS	:
{}


-----------------ROLE------------------
URL	:	roles/createRole
METHOD	:	POST
PARAMS	:
{
    "roleName": "admin"
}

URL	:	roles/getAllRoles
METHOD	:	GET
PARAMS	:
{}

URL	:	roles/deleteRole
METHOD	:	POST
PARAMS	:
{
    "roleName": "admin"
}

-----------------Organisation------------------
URL	:	/organisations/addOrganisation
METHOD	:	POST
PARAMS	:
{
    "name": "name",
    "email": "email",
    "pan": "pan",
    "tan": "tan",
    "country": "country",
    "state": "state",
    "city": "city",
    "address": "address",
    "landline": "landline",
    "fax": "fax",
    "mobile": "mobile",
    "whatsapp": "true",
    "alternateMobile": "alternateMobile",
    "alternateWhatsapp": "true",
    "planType": "planType",
    "planStartDate": "date",
    "planExpiryDate": "date"
}

URL	:	/organisations/getAllOrganisations
METHOD	:	GET
PARAMS	:
{}

URL	:	/organisations/updateOrganisation
METHOD	:	POST
PARAMS	:
{
    orgId: '845120252010'
    name: 'name',
    email: 'email',
    pan: 'pan',
    tan: 'tan',
    country: 'country',
    state: 'state',
    city: 'city',
    address: 'address',
    landline: 'landline',
    fax: 'fax',
    mobile: 'mobile',
    whatsapp: 'true',
    alternateMobile: 'alternateMobile',
    alternateWhatsapp: 'true',
    planType: 'planType',
    planStartDate: 'date',
    planExpiryDate: 'date'
}

URL	:	/organisations/deleteOrganisation
METHOD	:	POST
PARAMS	:
{
    "orgId": "orgId"
}

URL	:	/organisations/viewOrganisation
METHOD	:	POST
PARAMS	:
{
    "orgId": "orgId",
}


~URL	:	/organisations/uploadLogo
METHOD	:	POST
PARAMS	:
{
    "orgId": "orgId",
    "img": "image"
}

-----------------Order------------------
URL	:	/orders/addOrder
METHOD	:	POST
PARAMS	:
{
    "orderName": "my flex",
    "representativeName": "Prashanth",
    "user": "userId",
    "customer": "customerId",
     "orderType": "star flex",
    "height": "125",
    "width": "125",
    "area": "125",
    "quantity": "125",
    "customDesign": true,
    "homeDelivery": true,
    "homeDeliveryAddress": "address",
    "pastingOnSite": true,
    "pastingOnSiteAddress": "address",
    "deliveryDate": "02/08/2017", // Date in Date format
    "totalPrice": "25000"
}

URL	:	/orders/getAllOrders
METHOD	:	GET
PARAMS	:
{}

URL	:	/orders/updateOrder
METHOD	:	POST
PARAMS	:
{
    "orderId": '845120252010',
    "orderName": "my flex imp",
    "representativeName": "Prashanth",
    "user": "userId",
    "customer": "customerId",
    "orderType": "star flex",
    "height": "125",
    "width": "125",
    "area": "125",
    "quantity": "125",
    "customDesign": true,
    "homeDelivery": true,
    "homeDeliveryAddress": "address",
    "pastingOnSite": true,
    "pastingOnSiteAddress": "address",
    "deliveryDate": "02/08/2017", // Date in Date format
    "totalPrice": "25000"
}

URL	:	/orders/deleteOrder
METHOD	:	POST
PARAMS	:
{
    "orderId": "321634168754168534"
}

URL	:	/orders/viewOrder
METHOD	:	POST
PARAMS	:
{
    "orderId": "9786461238974515",
}


URL	:	/orders/uploadLogo
METHOD	:	POST
PARAMS	:
{
    "orderId": "7985641651311354132",
    "img": "image Base64"
}