var ErrorCode = require('../helpers/error-code'),
    ErrorConstant = require('../helpers/error-constants'),
    errorHandler = require('./error-handler'),
    StringConstants = require('../helpers/string-constants');

module.exports = {

    handler: new errorHandler(function (error) {
        var response = {
            error: {
                code: ErrorCode[error.value],
                message: ErrorConstant[error.value]
            },
            data: ''
        };
        error.res.status(ErrorCode['HTTP_FAILED']).send(response);
    }),

    setErrorResponse: function (error, res) {
        var response = {
            error: {
                code: ErrorCode[error],
                message: ErrorConstant[error]
            },
            data: ''
        };
        return res.status(ErrorCode['HTTP_FAILED']).send(response);
    },
    setSuccessResponse: function (data, res) {
        var response = {
            error: {
                code: '',
                message: ''
            },
            data: data
        };
        return res.status(ErrorCode['HTTP_SUCCESS']).send(response);
    }
};
