var Express = require('express'),
    Router = Express.Router(),

    policy = require('../policies/policy'),

    orgController = require('../controllers/organisation.controller');

// Create
Router.route('/addOrganisation').all(policy.isAllowed).post(orgController.addOrganisation);
// Read
Router.route('/getAllOrganisations').all(policy.isAllowed).get(orgController.getAllOrganisations);
// Update
Router.route('/updateOrganisation').all(policy.isAllowed).post(orgController.updateOrganisation);
// Delete
Router.route('/deleteOrganisation').all(policy.isAllowed).post(orgController.deleteOrganisation);

Router.route('/viewOrganisation').all(policy.isAllowed).post(orgController.viewOrganisation);

Router.route('/uploadLogo').all(policy.isAllowed).post(orgController.uploadLogo);

module.exports = Router;
