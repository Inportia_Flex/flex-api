var Express = require('express'),
    Router = Express.Router(),

    policy = require('../policies/policy'),

    orderController = require('../controllers/order.controller');

// Create
Router.route('/addOrder').all(policy.isAllowed).post(orderController.addOrder);
// Read
Router.route('/getAllOrders').all(policy.isAllowed).get(orderController.getAllOrders);
// Update
Router.route('/updateOrder').all(policy.isAllowed).post(orderController.updateOrder);
// Delete
Router.route('/deleteOrder').all(policy.isAllowed).post(orderController.deleteOrder);

Router.route('/viewOrder').all(policy.isAllowed).post(orderController.viewOrder);

Router.route('/uploadDesignImage').all(policy.isAllowed).post(orderController.uploadDesignImage);

module.exports = Router;
