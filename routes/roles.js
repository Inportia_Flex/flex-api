var Express = require('express'),
    Router = Express.Router(),

    roleController = require('../controllers/role.controller');

Router.route('/createRole').all().post(roleController.createRole);

Router.route('/getAllRoles').all().post(roleController.getAllRoles);

Router.route('/deleteRole').all().post(roleController.deleteRole);

module.exports = Router;
