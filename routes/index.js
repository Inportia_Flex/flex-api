var Routes = require('require-dir')(),
    appConfig = require('../config/config');

module.exports = function (app) {
    var urlWithApiVersion = '/' + appConfig.apiVersion;
    Object.keys(Routes).forEach(function (route) {
        app.use(urlWithApiVersion + '/' + route, Routes[route]);
    });
};
