var Express = require('express'),
    Router = Express.Router(),

    policy = require('../policies/policy'),

    userController = require('../controllers/user.controller');

// Create
Router.route('/addUser').all().post(userController.addUser);
// Read
Router.route('/getAllUsers').all(policy.isAllowed).get(userController.getAllUsers);
// Update
Router.route('/updateUser').all(policy.isAllowed).post(userController.updateUser);
// Delete
Router.route('/deleteUser').all().post(userController.deleteUser);


Router.route('/login').all().post(userController.login);

Router.route('/logout').all().post(userController.logout);

Router.route('/changePassword').all().post(userController.changePassword);

Router.route('/forgotPassword').all().post(userController.forgotPassword);

Router.route('/resetPassword').all().post(userController.resetPassword);

Router.route('/confirm').all().get(userController.confirm);

Router.route('/activate/:id').all().get(userController.activateId);

Router.route('/deactivate/:id').all().get(userController.deactivateId);

Router.route('/viewProfile').all(policy.isAllowed).post(userController.viewProfile);

Router.route('/uploadPhoto').all(policy.isAllowed).post(userController.uploadPhoto);

Router.route('/countries').all(policy.isAllowed).get(userController.countries);

Router.route('/me').all().post(userController.me);

Router.route('/tokenCheck').all().post(userController.tokenCheck);

module.exports = Router;
