var Express = require('express'),
    Router = Express.Router(),

    policy = require('../policies/policy'),

    adminController = require('../controllers/admin.controller');

Router.route('/createRoles').all().get(adminController.createRoles);

Router.route('/createSuperAdmin').all().get(adminController.createSuperAdmin);

module.exports = Router;
