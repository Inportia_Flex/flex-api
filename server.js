var dotenv = require('dotenv').config();

var Express = require('express'),
    BodyParser = require('body-parser'),
    Passport = require('passport'),
    Mongoose = require('mongoose'),
    Morgan = require('morgan'),
    JWT = require('jsonwebtoken'),
    secrets = require('./config/secrets'),
    apiHandler = require('./services/api-handler'),
    appConfig = require('./config/config'),
    cors = require('cors');

Mongoose.Promise = global.Promise;
Mongoose.connect('mongodb://' + process.env.DB_HOST + '/' + process.env.DB_NAME);
Mongoose.connection;

var App = Express();

App.use(cors());

App.use(process.env.PROFILE_UPLOAD, Express.static('profileUploads'));

App.use(process.env.DESIGN_UPLOAD, Express.static('designs'));

App.use(BodyParser.json({limit: '50mb'}));

App.use(BodyParser.urlencoded({limit: '50mb', extended: true}));

App.use(Passport.initialize());
App.use(Passport.session());

App.use(Morgan('dev'));

App.use(function (req, res, next) {
    var excludedRoutes = appConfig.excludedRoutes,
        token;
    if (excludedRoutes.indexOf(req.path) === -1) {
        token = req.headers.token || req.body.token || req.headers['x-access-token'];
        if (token) {
            JWT.verify(token, secrets.jwtSecret, function (err, decoded) {
                if (err) {
                    apiHandler.setErrorResponse('INVALID_TOKEN', res);
                } else {
                    req.decoded = decoded;
                    req.user = decoded._doc;
                    next();
                }
            });
        } else {
            apiHandler.setErrorResponse('NO_TOKEN', res);
        }
    } else {
        next();
    }
});

require('./routes/index')(App);

App.set('port', (process.env.PORT));
App.set('host', (process.env.HOST));

App.listen(App.get('port'), function () {
    console.log('Server started at ' + App.get('host') + ':' + App.get('port'));
});
